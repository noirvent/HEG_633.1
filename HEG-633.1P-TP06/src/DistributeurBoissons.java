/**
 * TP06 633.1 Programmation Java
 * Distributeur de boissons
 * @author Jonathan Blum
 */
import java.awt.*;
import java.awt.event.*;

public class DistributeurBoissons extends Frame implements ActionListener, TextListener {
    // Constantes
    private static final String CURRENCY = "CHF";
    private static final String LB_CHOIX_BOISSON = "Boisson choisie";
    private static final String LB_PRIX_FINAL = "Prix à payer: ";
    private static final String LB_NB_SUCRES = "Nombre de sucres: ";
    private static final int PRIX_BOISSON_1 = 100; // Prix exprimés en centimes pour n'avoir à manipuler que des int
    private static final int PRIX_BOISSON_2 = 80;
    private static final int PRIX_SUCRE = 5;
    private static final int MAX_SUCRES = 4;
    // Variables globales
    private int nb_sucres = 0;
    // Composants graphiques
    private final Label lbNumBoisson = new Label(LB_CHOIX_BOISSON+"(1-10)");
    private final Label lbNbSucres = new Label();
    private final Label lbSelection = new Label(LB_CHOIX_BOISSON+": ?");
    private final Label lbPrixAPayer = new Label();
    private final TextField tfNumBoisson = new TextField(3);
    private final Button btAjouter = new Button("+");
    private final Button btEnlever = new Button("-");
    private final Button btFabBoisson = new Button("Fabriquer la boisson");
    private final Button btNouveau = new Button("Nouveau");
    private final Button btFermer = new Button("Fermer");
 
    private DistributeurBoissons(String titre) {
        super(titre);
        initInterface();
    }
    
    private void initInterface() {
        // Param. de la fenêtre
        setSize(500, 150);     
        setLayout(new FlowLayout());
        addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent we) {System.exit(0);}
            public void windowClosing(WindowEvent we) {((Frame)we.getSource()).dispose();}
        });
        // Référencement des composants
        add(lbNumBoisson);
        add(tfNumBoisson);
        add(lbNbSucres);
        add(btAjouter); 
        add(btEnlever);
        add(lbSelection);
        add(btFabBoisson);
        add(btNouveau);
        add(btFermer);
        add(lbPrixAPayer);
        // Listeners
        btAjouter.addActionListener(this);
        btEnlever.addActionListener(this);
        btNouveau.addActionListener(this);
        btFermer.addActionListener(this);
        btFabBoisson.addActionListener(this);
        tfNumBoisson.addTextListener(this);
        // État inititial des composants
        resetInterface();
    }

    private void resetInterface() {
        nb_sucres = 0;
        tfNumBoisson.setText("");
        lbNbSucres.setText(LB_NB_SUCRES+nb_sucres);
        lbSelection.setText(LB_CHOIX_BOISSON+": ?");
        lbPrixAPayer.setText(LB_PRIX_FINAL + "?");
        btFabBoisson.setEnabled(false);
        btAjouter.setEnabled(false);
        btEnlever.setEnabled(false);
        validate();
    }
    
    private void updateInterface() {
        lbNbSucres.setText(LB_NB_SUCRES + nb_sucres);
        checkButtonState();
        validate();
    }
    /**
     * Gestion des actions des boutons
     * @param ae ActionEvent
     */
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource().equals(btNouveau)) 
            resetInterface();
        if(ae.getSource().equals(btFermer))
            dispose();
        if(ae.getSource().equals(btAjouter))
            addSucre();
        if(ae.getSource().equals(btEnlever))
            removeSucre();
        if(ae.getSource().equals(btFabBoisson))
            fabriquerBoisson();
    }
    /**
     * Gestion des modifications des Textfields
     * @param te TextEvent
     */
    public void textValueChanged(TextEvent te) {
        if(te.getSource().equals(tfNumBoisson))
            validateSaisie();
    }
    /**
     * Change l'état des boutons en fonction des données
     */
    private void checkButtonState() {
        btFabBoisson.setEnabled((getNumBoisson() > 0));  
        boolean sucrable = isSucrable();
        btAjouter.setEnabled(sucrable && (nb_sucres<MAX_SUCRES));
        btEnlever.setEnabled(sucrable && (nb_sucres>0));        
    }

    private double calculerPrixBoisson() {
        int prix = (isSucrable()) ? PRIX_BOISSON_1 + nb_sucres*PRIX_SUCRE : PRIX_BOISSON_2;   
        return (double)prix/100; // conversion centimes->francs
    }

    private void fabriquerBoisson() {
        if(getNumBoisson() > 0) {
            lbSelection.setText(LB_CHOIX_BOISSON+":"+getNumBoisson()+((nb_sucres>0)? " avec " + nb_sucres + " sucre(s)":""));
            lbPrixAPayer.setText(LB_PRIX_FINAL + calculerPrixBoisson() + CURRENCY);    
            updateInterface(); 
        }           
    }

    private void addSucre() {
        if( isSucrable() && nb_sucres < 4) { 
            nb_sucres++;
            updateInterface();
        }       
    }

    private void removeSucre() {
        if( isSucrable() && nb_sucres > 0) { 
            nb_sucres--;
            updateInterface();
        }
    }
    private void validateSaisie() {
        nb_sucres = 0;
        checkButtonState();
        updateInterface();
    }
    /**
     * Vérifie la validité de la boisson demandée
     * @return Le n° de la boisson si valide, -1 si invalide
     */
    public int getNumBoisson() {
        try {
            int boisson = Integer.parseInt(tfNumBoisson.getText());
            return (1 <= boisson && boisson <= 10) ? boisson : -1;
        } catch(NumberFormatException e) {
            return -1;
        }
    }
    /**
     * Vérifie si la boisson est sucrable
     * @return true si sucrable, false sinon
     */
    public boolean isSucrable() {
        int boisson = getNumBoisson();
        return 1 <= boisson && boisson <= 7;   
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new DistributeurBoissons("Distributeur de boissons").setVisible(true);    
    } 
}