/**
 * Statistique sur les commissions réalisées par les vendeurs:
 * - Modélisation d'un vendeur.
 *
 * @author VOTRE NOM
*/
public class Vendeur {

  /* Identifiant: un vendeur est identifié par le couple (agence, num) */
  private String agence; /* Agence dans laquelle travaille le vendeur */
  private int num;       /* Numéro du vendeur */
  private int sommeTot;  /* Somme totale des commissions de ce vendeur */


  /***** À COMPLÉTER SI NÉCESSAIRE *****/
 
    public Vendeur(String agence, int num, int sommeTot) {
        this.agence = agence;
        this.num = num;
        this.sommeTot = sommeTot;
    }

    public String getAgence() {
        return agence;
    }

    public int getNum() {
        return num;
    }

    public int getSommeTot() {
        return sommeTot;
    }

    public void setSommeTot(int sommeTot) {
        this.sommeTot = sommeTot;
    }
    
    public void addCommission(int somme) {
        this.sommeTot += somme;
    }
    
    public String toString() {
        return agence + " vendeur n°" + num + " - " + sommeTot + " F.";
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vendeur other = (Vendeur) obj;
        if ((this.agence == null) ? (other.agence != null) : !this.agence.equals(other.agence)) {
            return false;
        }
        if (this.num != other.num) {
            return false;
        }
        return true;
    }


} // Vendeur
