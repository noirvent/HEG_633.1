import java.util.*;
/**
 * Statistique sur les commissions réalisées par les vendeurs
 *
 * @author Jonathan Blum
*/
public class StatVentes {

  /* L'ArrayList des résultats des vendeurs. Contient une seule occurence de chaque vendeur. 
     Les éléments stockés dans l'ArrayList sont des instances de la classe Vendeur. */
  private static ArrayList resultats = new ArrayList();

  /* Calcule la somme totale des commissions de chaque vendeur et stocke le résultat de ce calcul
     dans l'ArrayList resultats.
     . Le String str contient une image du fichier de données.
     . Algorithme (imposé):
       - Le String str est découpé en lignes avec un StringTokenizer; les séparateurs sont "\n\r".
       - Chaque ligne ainsi obtenue est découpée avec un StringTokenizer; le séparateur est " " (espace).
       - Le numéro de la semaine est ignoré.
       - Les autres informations concernent un vendeur donné, à savoir l'agence où il travaille, son
         numéro (ces deux informations IDENTIFIENT un vendeur) et les différentes commissions qu'il a
         réalisé sur ses ventes lors de la semaine (le nombre de ces commissions est variable).
       - Si le vendeur est déjà référencé dans l'ArrayList resultats, la somme des commissions est mise
         à jour avec ses résultats de la semaine.
       - Si le vendeur n'existe pas encore, une nouvelle instance de Vendeur est ajoutée à l'ArrayList
         résultats avec toutes les informations de la semaine (agence, numéro, somme des commissions). */
  private static void calculeSommeVendeurs (String str) {

    /***** À COMPLÉTER *****/
    StringTokenizer lines = new StringTokenizer(str, "\n\r");
    
    while(lines.hasMoreTokens()) {
        
        // Lecture la ligne
        StringTokenizer line = new StringTokenizer(lines.nextToken(), " ");
        
        Integer noSemaine = new Integer(line.nextToken());
        String agence = line.nextToken();
        Integer noEmploye = new Integer(line.nextToken());
        
        // addition des commissions
        int totalComm = 0;
        while(line.hasMoreTokens())  {
            totalComm += Integer.parseInt(line.nextToken());
        }
        
        Vendeur newVendeur = new Vendeur(agence, noEmploye.intValue(), totalComm);  
        
       
        // Vérifier si le vendeur existe
        boolean flagVendeurExiste = false;
        
        Iterator it = StatVentes.resultats.iterator();
        while (!flagVendeurExiste && it.hasNext()) {
            Vendeur vNext = (Vendeur) it.next();
            
            if(vNext.equals(newVendeur)) {
                // Vendeur existe, on met a jour et on quitte la boucle
                vNext.addCommission(totalComm);      
                flagVendeurExiste = true;
                
            }      
        }
        
        // Si vendeur inexistant, on ajoute
        if(!flagVendeurExiste) StatVentes.resultats.add(newVendeur);
    }
    

  } // calculeSommeVendeurs

  /* Calcule et retourne la moyenne des sommes totales des commissions des vendeurs. */
  private static double moyenne () {

    /***** À COMPLÉTER *****/
      double moyenne = 0;
      
      Iterator it = StatVentes.resultats.iterator();
      
      while(it.hasNext()) {
          moyenne += ((Vendeur)it.next()).getSommeTot();
      }
      
      return moyenne /= StatVentes.resultats.size();

  } // moyenne

  /* Calcule et retourne le vendeur dont la somme totale des commissions est la plus proche de moy. */
  private static Vendeur plusProche (double moy) {

    /***** À COMPLÉTER *****/
      
        Iterator it = StatVentes.resultats.iterator();
        
        Vendeur vPlusProche = (Vendeur) it.next();
        while(it.hasNext()) {
            Vendeur vNext = (Vendeur) it.next();
            
            vPlusProche = (Math.abs(moy-vPlusProche.getSommeTot()) < Math.abs(moy-vNext.getSommeTot()) ? vPlusProche : vNext);       
        }
      
        return vPlusProche;
        
  } // plusProche

  /* Calcule et affiche:
     - Le nombre de vendeurs différents, un message circonstancié s'il n'y en a aucun.
     - La moyenne des sommes totales des commissions des vendeurs.
     - Le vendeur dont la somme totale de commissions est la plus proche de la moyenne ainsi
       que la distance à la moyenne de sa somme de commissions.
     Les résultats dont affichés en respectant la présentation définie dans l'énoncé. */
  private static void afficheRes () {

    /***** À COMPLÉTER *****/
    System.out.println("Il y a " + StatVentes.resultats.size() + " vendeurs différents."); 
    if(StatVentes.resultats.size() > 0) {
        double moy = moyenne();
        Vendeur vPlusProche = plusProche(moy);
        System.out.println("La moyenne des sommes totales des commissions des vendeurs est de " + moy + " F.");
        System.out.println("Le vendeur le plus proche de la moyenne est: " + vPlusProche);
        System.out.println("La somme de ses commissions est de " + Math.abs(moy-vPlusProche.getSommeTot()) + " de la moyenne.");
        
    } else {
        System.out.println("Il n'y a donc aucune statistique à effectuer.");
    }

  } // afficheRes

  /* Méthode principale. */
  public static void main (String[] args) {
    if (args.length < 1) { 
    	System.out.println("Vous devez préciser le nom du fichier.");
    }	else {
      System.out.println("Traitement du fichier \"" + args[0] + "\"");
      calculeSommeVendeurs(FileToStr.read(args[0]));
      afficheRes();
    }
  } // main

} // StatVentes