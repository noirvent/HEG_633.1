/**
 *
 * @author Jonathan Blum
 */
import java.awt.*;
import java.awt.event.*;

public class PiecesDetachees {
    
    private static final String PRIX_BRUT = "Prix brut = ";
    private static final String RABAIS_QTE = "Rabais quantité = ";
    private static final String TVA = "TVA = ";
    private static final String PRIX_A_PAYER = "Prix à payer = ";
    private static final double RABAIS_50 = 0.03;
    private static final double RABAIS_100 = 0.05;
    private static final double RABAIS_1000 = 0.10;
    private static final double TVA_0 = 0.082;
    private static final double TVA_250 = 0.063;
    private static final double TVA_500 = 0.045;
    private static final String CURRENCY = "F";
    
    private static class PiecesDetacheesWindowListener extends WindowAdapter {
        public void windowClosed(WindowEvent we) {System.exit(0);}
        public void windowClosing(WindowEvent we) {((Frame)we.getSource()).dispose();}
    }
    private static class NouveauActionListener implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            tfNbPieces.setText("");
            tfPrixPiece.setText("");
            resetLabels();
        }
    }
    private static class ValidationChampsTextListener implements TextListener {
        public void textValueChanged(TextEvent te) {
            if(tfNbPieces.getText().equals("") || tfPrixPiece.getText().equals(""))
                btCalculer.setEnabled(false);
            else
                btCalculer.setEnabled(true);
        }
    }
    private static class CalculerActionListener implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            try {
                int nbPieces = Integer.parseInt(tfNbPieces.getText());
                double prixPiece = Double.parseDouble(tfPrixPiece.getText());
                if((nbPieces <= 0) || (prixPiece <= 0)) {
                    lbErreur.setText("Les valeurs entrées doivent être >  0");
                } else {
                    // Données valides, calculs
                    double total = nbPieces * prixPiece;
                    double rabais = CalculerRabais(nbPieces, total);
                    double tva = CalculerTva(nbPieces, total-rabais);
                    double totalnet = total - rabais + tva;
                    lbPrixBrut.setText(PRIX_BRUT + total+CURRENCY);
                    lbRabais.setText(RABAIS_QTE + rabais+CURRENCY);
                    lbTva.setText(TVA+tva+CURRENCY);
                    lbPrixAPayer.setText(PRIX_A_PAYER+totalnet+CURRENCY);                   
                }
            } catch (NumberFormatException ex) {
                lbErreur.setText("Nombre de pièces ou prix d'une pièce invalide");
            }
            f.validate();
        }
    }
    
    private static Frame f;
    
    private static final Label lbNbPieces = new Label("Nombre de pièces");
    private static final Label lbPrixPiece = new Label("Prix d'une pièce");
    private static final TextField tfNbPieces = new TextField(10);
    private static final TextField tfPrixPiece = new TextField(10);
    private static final Label lbPrixBrut = new Label(PRIX_BRUT + "?");
    private static final Label lbRabais = new Label(RABAIS_QTE + "?");
    private static final Label lbTva = new Label(TVA + "?");
    private static final Label lbPrixAPayer = new Label(PRIX_A_PAYER + "?");
    private static final Button btCalculer = new Button("Calculer");
    private static final Button btNouveau = new Button("Nouveau");
    private static final Label lbErreur = new Label("");
      
    public static void main(String[] args) {
        f = new Frame("Pièces détachées");
        f.setSize(300,200);
        f.setLayout(new FlowLayout());
        // Composants
        f.add(lbNbPieces);
        f.add(tfNbPieces);
        f.add(lbPrixPiece);
        f.add(tfPrixPiece);
        f.add(lbPrixBrut);
        f.add(lbRabais);
        f.add(lbTva);
        f.add(lbPrixAPayer);
        f.add(btCalculer); btCalculer.setEnabled(false);
        f.add(btNouveau);
        f.add(lbErreur);
        // Listeners
        f.addWindowListener(new PiecesDetacheesWindowListener());
        btNouveau.addActionListener(new NouveauActionListener());
        btCalculer.addActionListener(new CalculerActionListener());
        tfNbPieces.addTextListener(new ValidationChampsTextListener());
        tfPrixPiece.addTextListener(new ValidationChampsTextListener());
        
        f.setVisible(true);
    }
    private static void resetLabels() {
        lbPrixBrut.setText(PRIX_BRUT+"?");
        lbRabais.setText(RABAIS_QTE+"?");
        lbTva.setText(TVA+"?");
        lbPrixAPayer.setText(PRIX_A_PAYER+"?");
        lbErreur.setText("");      
    }

    private static double CalculerRabais(int nbPieces, double total) {
        double rabais = 0;       
        if(nbPieces >= 1000)
            rabais = total * RABAIS_1000;
        else if (nbPieces >= 100)
            rabais = total * RABAIS_100;
        else if (nbPieces >= 50)
            rabais = total * RABAIS_50;
        else
            rabais = 0;
        return rabais;
    }
    
    private static double CalculerTva(int nbPieces, double prixbrut) {
        double tva = 0;
        if(nbPieces <= 250)
            tva = prixbrut * TVA_0;
        else if (nbPieces <= 500)
            tva = prixbrut * TVA_250;
        else
            tva = prixbrut * TVA_500;
        return tva;
    }
}
