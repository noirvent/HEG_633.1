/**
 *
 * @author Jonathan Blum
 */
import java.awt.*;
import java.awt.event.*;

public class Rectangle {
    
    /* Constantes */
    private static final String AIRE = "Aire = ";
    private static final String PERI = "Périmètre = ";
    
    private static class RectangleWindowListener extends WindowAdapter {
        public void windowClosed (WindowEvent we) {System.exit(0);}
        public void windowClosing(WindowEvent we) {((Frame)we.getSource()).dispose();}
    }

    private static class CalculerActionLister implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            double h = Double.parseDouble(tfHauteur.getText());
            double l = Double.parseDouble(tfLargeur.getText());
            if((h < 0) || (l < 0)) {
                lbAire.setText("Les données sont invalides");
                lbPerimetre.setText("");
            } else {
                double aire = h * l;
                double peri = 2*h + 2*l;
                lbAire.setText(AIRE + aire);
                lbPerimetre.setText(PERI + peri);
            }
            f.validate();
        }
    }
    private static class NouveauActionLister implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            tfHauteur.setText("");
            tfLargeur.setText("");
            lbAire.setText(AIRE+ " ?");
            lbPerimetre.setText(PERI+ " ?");
        }
    }
    private static class FermerActionLister implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            f.dispose();
        }
    }
    private static class ValidateChampsTextListener implements TextListener {
        public void textValueChanged(TextEvent te) {
           try {
               Double.parseDouble(tfHauteur.getText());
               Double.parseDouble(tfLargeur.getText());
               btCalculer.setEnabled(true);
               
           } catch (NumberFormatException ex) {
               btCalculer.setEnabled(false);
           }
        }   
    }   
    
    private static Frame f;
  
    private static final Label lbHauteur = new Label("Hauteur");
    private static final Label lbLargeur = new Label("Largeur");
    private static final TextField tfHauteur = new TextField(20);
    private static final TextField tfLargeur = new TextField(20);
    private static final Label lbAire = new Label(AIRE + "?");
    private static final Label lbPerimetre = new Label(PERI + "?");
    private static final Button btCalculer = new Button("Calculer");
    private static final Button btNouveau = new Button("Nouveau");
    private static final Button btFermer = new Button("Fermer");
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        f = new Frame("Rectangle");
        f.setSize(500,125);
        f.setLayout(new FlowLayout());
        // Composants
        f.add(lbHauteur);
        f.add(tfHauteur);
        f.add(lbLargeur);
        f.add(tfLargeur);
        f.add(lbAire);
        f.add(lbPerimetre);
        f.add(btCalculer); btCalculer.setEnabled(false);
        f.add(btNouveau);
        f.add(btFermer);
        // Listeners
        f.addWindowListener(new RectangleWindowListener());
        btCalculer.addActionListener(new CalculerActionLister());
        btNouveau.addActionListener(new NouveauActionLister());
        btFermer.addActionListener(new FermerActionLister());
        tfHauteur.addTextListener(new ValidateChampsTextListener());
        tfLargeur.addTextListener(new ValidateChampsTextListener());
        f.setVisible(true);
        
    }
    
}
