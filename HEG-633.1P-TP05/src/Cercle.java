/**
 * Calcul de l'aire et du p�rim�tre d'un cercle � partir de la donn�e de son rayon.
 *
 * Programme construit en cours le 14.10.2013
 *
 * @author Peter DAEHNE - HEG-Gen�ve
 * @version Version 1.11
*/
import java.awt.*;
import java.awt.event.*;

public class Cercle {

  /* Constantes */
  private static final String AIRE = "Aire = ";
  private static final String PERI = "Périmètre = ";

  /* Listener g�rant la fermeture de la fen�tre */
  private static class MyWindowListener extends WindowAdapter {
    public void windowClosed (WindowEvent e) {System.exit(0);}
    public void windowClosing (WindowEvent e) {((Frame)e.getSource()).dispose();}
  } // MyWindowListener

  /* Listener g�rant l'appui sur le bouton "Calculer" */
  private static class CalculerActionListener implements ActionListener {
    public void actionPerformed (ActionEvent e) {
      double rayon = Double.parseDouble(tfRayon.getText());
      double aire = Math.PI * rayon * rayon;
      double perim = 2 * Math.PI * rayon;
      lblAire.setText(AIRE + aire);
      lblPerim.setText(PERI + perim);
      frm.validate();
    } // actionPerformed
  } // CalculerActionListener

  /* Listener g�rant l'appui sur le bouton "Fermer" */
  private static class FermerActionListener implements ActionListener {
    public void actionPerformed (ActionEvent e) {
      frm.dispose();
    } // actionPerformed
  } // FermerActionListener

  private static Frame frm; /* La fen�tre */

  /* Les composants de la fen�tre */
  private static Label lblRayon = new Label("Rayon");
  private static TextField tfRayon = new TextField(20);
  private static Label lblAire = new Label(AIRE + "?");
  private static Label lblPerim = new Label(PERI + "?");
  private static Button btnCalculer = new Button("Calculer");
  private static Button btnFermer = new Button("Fermer");

  public static void main (String[] args) {
    frm = new Frame("Cercle"); /* Cr�er la fen�tre */
    frm.setSize(300, 100);
    frm.setLayout(new FlowLayout()); /* D�finir la mise en page des composants */
    /* Ajouter les composants � la fen�tre */
    frm.add(lblRayon);    
    frm.add(tfRayon);
    frm.add(lblAire);
    frm.add(lblPerim);
    frm.add(btnCalculer);
    frm.add(btnFermer);
    /* D�finir le listener de la fen�tre */
    frm.addWindowListener(new MyWindowListener());
    /* D�finir le listener du bouton "Calculer" */
    btnCalculer.addActionListener(new CalculerActionListener());
    /* D�finir le listener du bouton "Fermer" */
    btnFermer.addActionListener(new FermerActionListener());
    frm.setVisible(true);
  } // main

} // Cercle