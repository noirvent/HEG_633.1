/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blum.heg.c633_1.algo;

import java.util.Random;

/**
 *
 * @author Jonathan Blum
 */
public class TableauScores{
    private int sizeMax = 0;
    private int size = 0;
    private Joueur[] t;

    public TableauScores(int taille) {
        sizeMax = taille;
        t = new Joueur[sizeMax];
    }
    
    /**
     * Copy Constructor
     * @param copy
     */
    public TableauScores(TableauScores copy) {
        sizeMax = copy.getSizeMax();
        size = copy.getSize();
        t = new Joueur[sizeMax];
        for (int i = 0; i < copy.getSizeMax(); i++) {
                t[i] = copy.get(i);
        }
    }
    public int getSizeMax() {return sizeMax;}
    private void setSizeMax(int sizeMax) {this.sizeMax = sizeMax;}
    public int getSize() {return size;}
    private void setSize(int size) {this.size = size;}
    public Joueur[] getT() {return t;}
    public Joueur get(int i) {return t[i];}
    public void setT(Joueur[] t) {this.t = t;}   
    public void add(Joueur j) {
        t[this.size] = j;
        this.size++;
    }
    
    public void shuffle() {
        int index; Joueur tmp;
        Random random = new Random();
        for(int i = this.size-1; i>0; i--) {
            index = random.nextInt(i+1);
            tmp = t[index];
            t[index] = t[i];
            t[i] = tmp;
        }
    }
    
    public void print() {
        for (int i = 0; i < this.sizeMax; i++) System.out.println(i+": " +t[i]);
    }
    
    /***************************************************************************
     * Insertions Ordonnées (Série A05)
     **************************************************************************/
    
    /**
     * Recherche dichotomique simple 
     */
    public void Insert20(Joueur j) {  
        int g, d, m, pos;
        int newSize = this.size +1; // Nouvelle taille du tableau
        if ( (newSize == 1 ) || (t[0].compareTo(j) < 0) ) {     // Si tableau pas vide & pas avant la première clef
            pos = 0;
        } else if (t[newSize-2].compareTo(j) >= 0) { // Si >= à la dernière valeur, on insert APRES
            pos = newSize-1;
        } else {    // Dans l'interval
            g = 0; d= newSize-2;
          
            // Invariant : g(indice) <= pos < d(indice) -> g < d -> g+1 <= d
            while ((g+1) < d) {
                m = (g+d) / 2;
                if(j.compareTo(t[m]) <= 0) {
                    g = m;
                } else {
                    d = m;
                }
            }
            pos = d;
        }
        // On décale... 
        for (int i = newSize-1; i >= pos; i--)
            t[i+1] = t[i];
        // On insert
        t[pos] = j;
        this.size++; // Pas oublier d'incrémenter le compte effectif
    }    
    // Attribution de valeur fictives au cellules non consultées 
    /* Justification 
     * De notre tableau, seul la case dont l'indice est m est consultée.
     * Celle-ci étant comprise au sein de notre interval qui va progressivement
     * se ressérer sur m, on est donc sur que les bornes "inexistantes",
     * ne seront jamais consultées. */
    public void Insert21(Joueur j) {
        int g, d, m, pos;
        int newSize = this.size +1; // Nouvelle taille du tableau   
        
        g = -1;
        d = newSize -1;
        // Invariant : g <= pos-1 < d; donc g < d; donc g+1 <= d
        while(g+1 < d) {
            m = (g+d) / 2;
            if(j.compareTo(t[m]) <= 0) {
                g=m;
            } else {
                d=m;
            }
        }
        pos = d;
        for(int i = newSize-1; i >= pos; i--)
            t[i+1] = t[i];
        t[pos] = j;
        this.size++; // Pas oublier d'incrémenter le compte effectif
    }
    /*
    public void InsertPre3(Joueur j) {
        int g, d, m, pos;
        int newSize = this.size +1; // Nouvelle taille du tableau   
        
        g-1 = -1;
        d+1 = newSize -1;
        // Invariant : g <= pos-1 < d; donc g < d; donc g+1 <= d
        while(g-1+1 < d+1) {
            m = (g-1+d+1) / 2;
            if(j.compareTo(t[m]) <= 0) {
                g-1=m;
            } else {
                d+1=m;
            }
        }
        pos = d+1;
        for(int i = newSize-1; i >= pos; i--)
            t[i+1] = t[i];
        t[pos] = j;
        this.size++; // Pas oublier d'incrémenter le compte effectif      
    }
    */
    public void Insert3(Joueur j) {
        int g, d, m, pos;
        int newSize = this.size +1; // Nouvelle taille du tableau   
        
        g = 0;
        d = newSize -2;
        // Invariant : g <= pos-1 < d; donc g < d; donc g+1 <= d
        // g-1+1 < d+1 donc g-1 < d donc g < d+1 donc g <= d
        while(g <= d) {
            m = (g+d) / 2;
            if(j.compareTo(t[m]) <= 0) {
                g=m+1;
            } else {
                d=m-1;
            }
        }
        pos = d+1; // équivalent à g
        for(int i = newSize-1; i >= pos; i--)
            t[i+1] = t[i];
        t[pos] = j;
        this.size++; // Pas oublier d'incrémenter le compte effectif          
    }
    
    /***************************************************************************
     * Recherche (Série A05)
     **************************************************************************/
    public int Mauchly(Joueur j) {
        int g, d, m, pos;
        int newSize = this.size +1; // Nouvelle taille du tableau  
        
        g = 0;
        d = newSize -2;
        // recherche
        while(g<=d) {
            m = (g+d) /2;
            if(j.compareTo(t[m]) <= 0) {
                g = m+1;
            } else {
                d = m-1;
            }
        }
        // Mais existe-t'il ou faut-il insérer ?
        if(d >= 0 && t[d].compareTo(j)== 0)
            return d;
        else
            return -g-1;
    }
    
    /***************************************************************************
     * Tris  (Série A06)
     **************************************************************************/
    public void tri20() {
        int g, d, pos, m;  
        Joueur j;
        
        for(int i=0; i < this.size; i++) {
            j = t[i];
            if(i+1 == 1 || t[0].compareTo(j) < 0) {     // Tab vide ou < first
                pos = 0;
            } else if (t[i-1].compareTo(j) >= 0) {      // >= last
                pos = i;
            } else {                                    // dans tab
                g = 0;
                d = i-1;
                while (g+1 < d) {                      
                    m = (g+d)/2;
                    if(j.compareTo(t[m]) <= 0)
                        g = m;
                    else
                        d = m;
                }
                pos = d;
            }
            for(int i2 = i-1; i2 >= pos; i2--) 
                t[i2+1] = t[i2];
            t[pos] = j;
        }
    } // tri20
    
    public void tri21() {
        int g, d, pos, m;  
        Joueur j;
        
        for(int iJoueur = 0; iJoueur < this.size; iJoueur++) {
            j = t[iJoueur];
            g = -1;
            d = iJoueur;
            
            while (g+1 < d) {
                m = (g+d)/2;
                if(j.compareTo(t[m]) <= 0) 
                    g=m;
                else
                    d=m;                    
            }
            pos = d;
            for(int i = iJoueur-1; i >= pos; i--)
                t[i+1] = t[i];
            t[pos] = j;
        }
    } // tri 21
    
    public void tri3() {
        int g, d, pos, m;
        Joueur j;
        
        for(int iJoueur = 0; iJoueur < this.size; iJoueur++) {
            j = t[iJoueur];
            g = 0;
            d = iJoueur-1;
            while(g<=d) {
                m = (g+d)/2;
                if(j.compareTo(t[m]) <= 0)
                    g = m+1;
                else
                    d = m+1;
            }
            pos=g;
            
            for(int i = iJoueur-1; i>= pos; i--)
                t[i+1] = t[i];
            //t[pos] = j;
        }
    } // tri3

    /***************************************************************************
     * Tris 2 (Série A07)
     **************************************************************************/
    public void triPlusPetit() {
        int nbtests = 0, nbswap = 0;
        
        int iPetit; Joueur tmp;
        for(int i=0; i<this.size; i++) {
            iPetit = i;
            for(int j=i; j<this.size; j++)
                iPetit = (t[iPetit].compareTo(t[j]) < 0) ? j : iPetit; nbtests++;
            if(iPetit > i) {
                tmp = t[i];
                t[i] = t[iPetit];
                t[iPetit] = tmp;
                nbswap++;
            } nbtests++;
        }
        
        System.out.println("triPlusPetit : " + nbtests + " tests, "+nbswap+ " échanges");        
    } // triPlusPetit    
    
    public void triPlusPetitEtPlusGrand() {
        int nbtests = 0, nbswap=0;
        
        int iPetit,
            iGrand,
            g = 0, 
            d = this.size-1,
            zqrat = this.size;
        Joueur tmp;
        
        while(zqrat > 0) {
            iPetit = g;
            iGrand = d;
            for(int i=g;i<=d;i++) {
                iPetit = (t[iPetit].compareTo(t[i]) < 0) ? i : iPetit; nbtests++;
                iGrand = (t[iGrand].compareTo(t[i]) > 0) ? i : iGrand; nbtests++;
                
            }        
            zqrat-=2;
            if(iPetit > g) {
               tmp = t[g];
               t[g] = t[iPetit];
               t[iPetit] = tmp;
               nbswap++;
            } nbtests++;
            
            if(iGrand < d) {
               tmp = t[d];
               t[d] = t[iGrand];
               t[iGrand] = tmp; 
               nbswap++;
            } nbtests++; 
            zqrat -= 2;
            g++;
            d--;
        }
        System.out.println("triPlusPetitEtPlusGrand : " + nbtests + " tests, "+nbswap+ " échanges");
    } // triPlusPetitEtPlusGrand    
    
    public void ShakerSort() {
        int nbtests = 0,nbswap = 0;
        boolean flagSwap = true;
        Joueur tmp;
        
        while(flagSwap) {
            flagSwap = false;
            for(int i = 0; i <= this.size-2; i++) {
                if(t[i].compareTo(t[i+1]) < 0) {
                  tmp = t[i];
                  t[i] = t[i+1];
                  t[i+1] = tmp;
                  flagSwap = true;
                  nbswap++;
                } nbtests++;
            }
            for(int i= this.size-2; i >= 0; i--) {
                if(t[i].compareTo(t[i+1]) < 0) {
                  tmp = t[i];
                  t[i] = t[i+1];
                  t[i+1] = tmp;
                  flagSwap = true;
                  nbswap++;
                } nbtests++;            
            }
        }    
        System.out.println("ShakerSort : " + nbtests + " tests, "+nbswap+ " échanges");  
    }// ShakerSort
}
