import java.util.*;
/**
 * Module 633.1-Programmation - TP P04
 *
 * Application parking silo
 *
 * Modélisation du concept "deux roues", c'est pourquoi cette classe est abstraite.
 *
 * VOUS NE DEVEZ PAS MODIFIER LE CODE DE CETTE CLASSE
*/
public abstract class DeuxRoues extends Vehicule {

  /* Constructeur (observez l'utilisation du constructeur parent) */
  public DeuxRoues (String immatriculation, String nom, String prenom, int type) {
    super (immatriculation, nom, prenom, type);
  } // Constructeur     
    
  /** 
   * Présentation des données spécifiques à la catégorie deux roues
   * Utilisation du toString() du parent pour composer la chaîne de caractères
  */
  public String toString () {
    return super.toString() + " deux roues";
  } // toString
    
} // DeuxRoues
