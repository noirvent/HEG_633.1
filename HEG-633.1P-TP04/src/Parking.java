import java.util.*;
/**
 * Module 633.1-Programmation - TP P04
 *
 * Application parking silo
 *
 * Modélisation du parking. Un parking est composé d'étages
 * Mémoriasation de la liste des référence sur des objets de type classe Etage
 *
 * @author Jonathan Blum
*/
public class Parking {
    
  /* Prix par type de véhicule (CHF / quart d'heure) */  
  public static final double PRIX_NON_DEFINI = 0.0;
  public static final double PRIX_VELO = 0.25; 
  public static final double PRIX_MOTO = 0.75; 
  public static final double PRIX_VOITURE = 3.0; 
    
  /* Attributs */ 
  private ArrayList etages;
    
  /* Constructeur */ 
  public Parking () {
    etages = new ArrayList();
  } // Constructeur  
   
  /* Accesseurs */ 
  public ArrayList getEtages () {return etages;}
  public Etage getEtage (int noEtage) {return (Etage)etages.get(noEtage);}

  /**
   * Retourne le prix au 1/4 d'heure en fonction du type
   * @param type
   * @return double prix au 1/4 d'heure
   */
  public static double getPrixByType(int type) {
      switch(type) {
          case Vehicule.TYPE_MOTO: return Parking.PRIX_MOTO;
          case Vehicule.TYPE_VOITURE: return Parking.PRIX_VOITURE;
          case Vehicule.TYPE_VELO: return Parking.PRIX_VELO;
          default: return Parking.PRIX_NON_DEFINI;
      }
  }
  
  
  /* Ajoute un étage */
  public void add (Etage etage) {etages.add(etage);}
   
  /* Retourne le nombre de véhicules (somme de tous les types de véhicules de l'étage) */ 
  public int getNbVehicules () {
    return getNbVehicules(Vehicule.TYPE_NON_DEFINI);
  } // getNbVehicules
  
  public int getNbVehicules (int type) {
    int total = 0;
    for (Iterator ite = etages.iterator(); ite.hasNext();)
        total += ((Etage) ite.next()).getNbVehicules(type);   
    return total;
  } // getNbVehicules   
  
  /* Retourne le montant (sommes des montants de tous les types de véhicules de l'étage) */ 
  public double getMontant () {
    return getMontant(Vehicule.TYPE_NON_DEFINI);
  } // getMontant
  
  public double getMontant (int type) {
    double total = 0f;
      for (Iterator ite = etages.iterator(); ite.hasNext();)
          total += ((Etage) ite.next()).getMontant(type);
    return total;
  } // getMontant
     
  
  /* Supprime le box contenant la voiture désignée par son IdVehicule
  * Tester tous les étages jusqu'à trouver le box contenant le véhicule
  * Retourne vrai si l'opération s'est bien déroulée, faux sinon
  */  
  public boolean removeVehicule (String idVehicule) {
    Iterator it = etages.iterator();
    for (Iterator ite = etages.iterator(); ite.hasNext();)
        if(((Etage) ite.next()).removeVehicule(idVehicule))
            return true;
    return false;
  } // removeVehicule   
  
  public void addBox(Box box, int numEtage, int duree) {
      box.setNbQuartHeures(duree);
      Etage e = new Etage(numEtage);
      int iEtage = etages.indexOf(e);
      if(iEtage >= 0)
        e = this.getEtage(iEtage);
      else
        this.add(e);
      e.add(box);
  }
   
  /* Présentation des données spécifiques au parking */
  public String toString () {
    return "Parking ";
  } // toString    
  
        
} // Parking
