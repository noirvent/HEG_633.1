import java.util.*;
/**
 * Module 633.1-Programmation - TP P04
 *
 * Application parking silo
 *
 * Modélisation d'un "étage" contenant les box.
 * Mémorisation du n° de l'étage et de la liste des références sur des objets de type classe Box
 *
 * @author Jonathan Blum
*/
public class Etage {
    
  /* Attributs */ 
  private int no;
  private ArrayList boxes;
    
  /* Constructeur (pas de constructeur sans paramètres) */ 
  public Etage (int no) {
    this.no = no;
    boxes = new ArrayList(); // Création effective de la liste des box
  } // Constructeur  
     
  /* Accesseurs */ 
  public int getNo () {return no;}
  public ArrayList getBoxes () {return boxes;}
  public Box getBox (int noBox) {return (Box)boxes.get(noBox);}

  /* Ajoute un box */
  public void add (Box box) {boxes.add(box);}  
   
  /* Retourne le nombre de véhicules (somme de tous les types de véhicules) */ 
  public int getNbVehicules () {
    return getNbVehicules(Vehicule.TYPE_NON_DEFINI);
  } // getNbVehicules
  
   public int getNbVehicules (int type) {
    int total = 0;
    if(type == Vehicule.TYPE_NON_DEFINI) 
        total = boxes.size();
    else
        for (Iterator itb = boxes.iterator(); itb.hasNext();)
            if(((Box) itb.next()).getVehicule().getType() == type) total++;       
    return total;
  } // getNbVehicules 
   
  /* Retourne le montant total (sommes des montants de tous les types de véhicules) */ 
  public double getMontant () {
    return getMontant(Vehicule.TYPE_NON_DEFINI);
  } // getMontant
  
  public double getMontant (int type) {
    double total = 0f;
      for (Iterator itb = boxes.iterator(); itb.hasNext();) {
          Box b = (Box) itb.next();
          if(b.getVehicule().getType() == type)
            total += Parking.getPrixByType(type) * b.getNbQuartHeures();
      }
    return total;
  } // getMontant
     
  /* Supprime le box contenant le véhicule designé par son IdVehicule
  * Retourne : vrai si l'opération s'est bien déroulée, faux sinon
  */  
  public boolean removeVehicule (String idVehicule) {
    if(!estVide()) {
        for (Iterator itb = boxes.iterator(); itb.hasNext();) {
            Box next = (Box) itb.next();
            if(next.getIdVehicule().equals(idVehicule)) {
                boxes.remove(next);
                return true;
            }
        } 
    }
    return false;
  } // removeVehicule
   
  /* Retourne vrai s'il n'y a pas de véhicule sur l'étage */
  public boolean estVide () {return boxes.isEmpty();}
   
  /* Présentation des données spécifiques au parking */
  public String toString () {
    return "Etage no : " + no;
  } // toString  
   
  public boolean equals (Object obj) {
    return (((Etage)obj).getNo() == getNo());
  } // equals  
} // Etage
