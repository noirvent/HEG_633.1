import java.util.*;
/**
 * Module 633.1-Programmation - TP P04
 *
 * Application parking silo
 *
 * Modélisation du véhicule deux roues "Moto".
 * La mémorisation est (héritée de Vehicule) : 
 *      - immatriculation (id);
 *      - nom et prénom du conducteur;
 *      - type de véhicule.
 *
 * VOUS POUVEZ MODIFIER LE CODE DE CETTE CLASSE SI VOUS LE SOUHAITEZ
*/
public class Moto extends DeuxRoues {
    
  /* Constructeur (observez l'utilisation du constructeur parent) */
  public Moto (String immatriculation, String nom, String prenom) {
    super (immatriculation, nom, prenom, Vehicule.TYPE_MOTO);
  } // Constructeur    
    
  /** 
   * Présentation des données spécifiques à la moto
   * Utilisation du toString() du parent pour composer la chaîne de caractères
   * 
   */
  public String toString () {
    return super.toString() + " conduit une moto ";
  } // toString
  
} // Moto
