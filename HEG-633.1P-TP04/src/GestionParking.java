import java.util.*;

/**
 * Module 633.1-Programmation - TP P04
 *
 * Application parking silo
 *
 * Gestion du parking silo : 
 *     mémorisation des données;
 *     présentation du rapport;
 *     retirer un ensemble de véhicules;
 *     présentation du nouveau rapport.
 *
 * @author Jonathan Blum
*/
public class GestionParking {
    
  /* Liste des délimiteurs */
  private static final String DEL_ETAGE = "\r\n";
  private static final String DEL_CHAMP = ";";
  private static final String DEL_NO_BOX = "-";
    
  /* Le parking */
  private static Parking parking; 
  
  /* Méthodes de service */
  private static int str2int (String s) {return Integer.parseInt (s);} 
  
  /* Mémorise les données du parking */
  private static void memoriseParking (String donnesParking) {
    /* Création du parking */  
    parking = new Parking(); 
    for (StringTokenizer data = new StringTokenizer(donnesParking, DEL_ETAGE); data.hasMoreTokens();) {
        StringTokenizer line = new StringTokenizer(data.nextToken(), DEL_CHAMP);
        StringTokenizer pos = new StringTokenizer(line.nextToken(), DEL_NO_BOX); // Etage-Box
        int numEtage = str2int(pos.nextToken());                                // Etage
        int numBox = str2int(pos.nextToken());                                  // Box
        int typeVehicule = str2int(line.nextToken());                           // Type de vehicule
        int time = str2int(line.nextToken());                                   // Temps en 1/4 d'heures
        String prenom = line.nextToken();                                       
        String nom = line.nextToken();
        String immatriculation = line.nextToken();                              // N° de plaque        
        Vehicule v = createVehiculeByType(typeVehicule, immatriculation, nom, prenom);
        if(v != null) parking.addBox(new Box(numBox, v), numEtage, time);   
    }
  } // memoriseParking    
  
  /**
   * Créé un véhicule en fonction de son type
   * @param typeVehicule    Le type du véhicule
   * @param immatriculation Son n° de plaque minéralogique
   * @param nom             Nom du propriétaire
   * @param prenom          Prénom du propétaire
   * @return Vehicule       Un véhicule
   */
  private static Vehicule createVehiculeByType(int typeVehicule, String immatriculation, String nom, String prenom) {
      Vehicule v;
      switch(typeVehicule) {
          case Vehicule.TYPE_MOTO: v = new Moto(immatriculation, nom, prenom); break;
          case Vehicule.TYPE_VELO: v = new Velo(immatriculation, nom, prenom); break;
          case Vehicule.TYPE_VOITURE: v = new Voiture(immatriculation, nom, prenom); break;
          default: v = null; break;
      }
      return v;
  }
  
  /* Prépare et affiche le rapport */
  private static void afficheRapport () {
    System.out.println("Rapport parking");
    System.out.println("Il y a " + parking.getNbVehicules() + " véhicules parqués");
    System.out.println("\t VELO : " + parking.getNbVehicules(Vehicule.TYPE_VELO) + " pour un montant actuel de : " + parking.getMontant(Vehicule.TYPE_VELO) + " CHF");
    System.out.println("\t MOTO : " + parking.getNbVehicules(Vehicule.TYPE_MOTO) + " pour un montant actuel de : " + parking.getMontant(Vehicule.TYPE_MOTO) + " CHF");
    System.out.println("\t VOITURE : " + parking.getNbVehicules(Vehicule.TYPE_VOITURE) + " pour un montant actuel de : " + parking.getMontant(Vehicule.TYPE_VOITURE) + " CHF");
    System.out.println("----------------------------------------------------------"); 
    System.out.println("Liste des immatriculations et des conducteurs"); 
    for (Iterator ite = parking.getEtages().iterator(); ite.hasNext();)
        for (Iterator itb = ((Etage)ite.next()).getBoxes().iterator(); itb.hasNext();)
            System.out.println(((Box)itb.next()).getVehicule());
  } // afficheRapport  
  
  /* VOUS NE DEVEZ PAS MODIFIER LE CODE DE CETTE METHODE */
  private static void recuperationVehicules () {
    System.out.println("----------------------------------------------------------");
    parking.removeVehicule("GE657665");
    parking.removeVehicule("NE342078");
    parking.removeVehicule("AG456");
    parking.removeVehicule("BE87350");
    parking.removeVehicule("NE563458");
    parking.removeVehicule("VD15646");
    
    System.out.println("Les véhicules suivants ont été supprimés :");
    System.out.println("   GE657665, NE342078, AG456, BE87350, NE563458 et VD15646");
    System.out.println("----------------------------------------------------------");
  } // recuperationVehicules
  
  /** 
   * C'est ici que tout commence ...
   * 
   * @param args nom du fichier des données
   */
  public static void main (String[] args) {
    if (args.length < 1) { 
    	System.out.println("Il faut saisir le fichier de données.");
    } else {
      System.out.println("Memorisation du parking (fichier : \"" + args[0] + "\")");
      System.out.println("----------------------------------------------------------");
      memoriseParking(FileToStr.read(args[0]));
      afficheRapport();
      recuperationVehicules();
      afficheRapport();
    }
  } // main   
    
} // GestionParking