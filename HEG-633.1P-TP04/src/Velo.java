import java.util.*;
/**
 * Module 633.1-Programmation - TP P04
 *
 * Application parking silo
 *
 * Modélisation du véhicule deux roues "Velo".
 * La mémorisation est (héritée de Vehicule) : 
 *      - immatriculation (id);
 *      - nom et prénom du conducteur;
 *      - type de véhicule.
 *
 * VOUS POUVEZ MODIFIER LE CODE DE CETTE CLASSE SI VOUS LE SOUHAITEZ
*/
public class Velo extends DeuxRoues {
    
  /* Constructeur (observez l'utilisation du constructeur parent) */
  public Velo (String immatriculation, String nom, String prenom) {
    super (immatriculation, nom, prenom, Vehicule.TYPE_VELO);
  } // Constructeur    
    
  /** 
   * Présentation des données spécifiques au vélo
   * Utilisation du toString() du parent pour composer la chaîne de caractères
   * 
   */
  public String toString () {
    return super.toString () + " conduit un vélo ";
  } // toString
  
} // Velo
