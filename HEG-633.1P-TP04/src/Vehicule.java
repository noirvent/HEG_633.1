import java.util.*;
/**
 * Module 633.1-Programmation - TP P04
 *
 * Application parking silo
 *
 * Modélisation du concept "véhicule", c'est pourquoi cette classe est abstraite.
 *
 * VOUS NE DEVEZ PAS MODIFIER LE CODE DE CETTE CLASSE
*/
public abstract class Vehicule {
   
  /* Types de véhicules (observez l'utilisation de static) */  
  public static final int TYPE_NON_DEFINI = -1;
  public static final int TYPE_VELO = 0; 
  public static final int TYPE_MOTO = 1; 
  public static final int TYPE_VOITURE = 2; 
    
  /* Attributs */
  protected String immatriculation;
  protected String nom;
  protected String prenom;
  protected int type;
    
  /* Constructeurs (pas de constructeur sans paramètre) */
  public Vehicule (String immatriculation, String nom, String prenom, int type) {
    this.immatriculation = immatriculation;
    this.nom = nom;
    this.prenom = prenom;
    this.type = type;
  } // constructeur
    
  /* 
   * Ce constructeur permet de construire un véhicule unique (avec son id immatriculation) 
   * avec des valeurs par défaut. 
   * Ce constructeur est spécifiquement utilisé pour les recherches et comparaisons dans les collections 
   * (Observez l'utilisation du constructeur this(...))
   */
  public Vehicule (String immatriculation) {
    this (immatriculation, "", "", Vehicule.TYPE_NON_DEFINI);
  } // constructeur   
    
  /* Accesseurs */
  public int getType () {return type;} 
  public String getId () {return immatriculation;} 
    
  /** Deux Vehicules sont identiques si leurs immatriculations sont identiques */
  public boolean equals (Object obj) {
    return this.immatriculation.equals(((Vehicule)obj).immatriculation);
  } // equals    
    
  /** 
  * Présentation des données spécifiques au véhicule
  */
  public String toString () {
    return "Véhicule : " + immatriculation + ", " + prenom + " " + nom;
  } // toString    
     
} // Vehicule
