/**
 *
 * @author jonathan
 */

class Personne {
   String nom;
   int num;
   
   Personne(String nom, int num) {
       this.nom = nom;
       this.num = num;
   }// Constructeur (String, int)
   
    public int compareTo(Object obj){
      Personne p = (Personne)obj;
      int resCompNoms = p.nom.compareToIgnoreCase(nom);
      if (resCompNoms != 0) {return resCompNoms;}
      return num - p.num;  //(attribut supposé limité)
    }//compareTo
    
    public String toString(){
      return "Nom : " + nom + " ;  numéro : " + num + ".";
    }// toString    

}// Personne

class TabOrdPers {
    Personne[] personnes;
    int maxSize;
    int size;
    
    TabOrdPers(int size) {
        personnes = new Personne[size];
        maxSize = size;
        this.size = 0;
    }
    
    public String toString() {
        String str = "";
        for(int i = 0; i< size;i++)
            str += "(" + personnes[i] +")";
        return str;
    }
    
    public boolean sorted(){
           if(size <= 1){
                   return true;
           }
           Personne precPers = personnes[0];
           for(int i = 1; i < size; i++){
                   Personne currentPers = personnes[i];
                   if(precPers.compareTo(currentPers) < 0){
                           return false;
                   }
                   precPers = currentPers;
           }
       return true;
    }// sorted
    
    public void insert(Personne p) {
        personnes[size] = p;
        size++;
    }
    
    public void listerPersonnes() {
        for(int i=0; i<size; i++)
            System.out.println("Personne " + i + " : " + personnes[i]);
    }
    
    /* Corrigé de Insert000 en Java */
    public void insert000_corr(Personne p){ 
            size++;
            int i = size - 2; 
            while(i >= 0 && personnes[i].compareTo(p) < 0) {
                    personnes[i+1] = personnes[i];
                    i--;
            }
            personnes[i+1] = p;
    }//insert000_corr
    
    /* Amélioration de la méthode élémentaire selon le principe de la sentinelle.
                    Grâce à une comparaison préliminaire, la 1re clé (pour autant qu'elle existe !) peut servir
                    de sentinelle.

                    Évaluez l'ordre de grandeur du coût moyen:
                    Un premier test doit être effectué pour vérifié que le tableau ne soit pas vide, ce test n'a en revenche qu'un coût très faible.
                    Ensuite il faut aussi tester si la première personne du tableau est plus petite que la nouvelle, un compareTo() peut coûter cher, cependant:
                            - Si la réponse est négative, on sait alors que le nouvelle élément ira au début du tableau, il suffit alors dedécaler les autres pour le placer:
                                    la boucle n'a donc qu'un faible coût et plus aucun compareTo() à faire -> un gain de performance par rapport à insert000 éxiste belle et bien
                            - Si la réponse est positive, on doit chercher où placer le nouvelle élément, mais notre comparaison préliminaire permet d'employer
                                    une  "sentinelle". Le gain de performance par rapport à insert000() n'est pas énorme, mais il éxiste belle et bien
                                    car on est sur une boucle simple (à une seule condition).
    */    
    public void insert001(Personne p){ 
            size++;		
            if(size <= 1){
                    personnes[0] = p;
                    return;
            } 	
            int i = size - 2; 
            if(personnes[0].compareTo(p) >= 0) { // Si le nouvelle élément est plus grand que le 1er du tableau
                    while(personnes[i].compareTo(p) <= 0) {	 // On utilise la sentinelle: on décale tout le monde vers la droite jusqu'à arriver à 				
                            personnes[i+1] = personnes[i];				// une personne plus petite ou équivalente au nouvelle élément
                            i--;
                    } 			
            } else { // Sinon, on place le nouvelle élément au début du tableau
                    while(i > -1) {	// On décale tout le monde d'un cran à droite				
                            personnes[i+1] = personnes[i];
                            i--;
                    }			
            }
            personnes[i+1] = p;	// On met le nouvelle élément à sa place
    }// insert001   
    public void insert010(Personne p){ 
            size++;
            int index = indexPersonne(p);
            for(int i = size - 1; i >= index; i--){ // On connait l'index, donc utiliser une boucle for est plus optimale...
                    personnes[i+1] = personnes[i];
            }	
            personnes[index] = p; // ... il ne reste plus qu'a placé le nouvelle élément à sa place
    }// insert010   
    
    private int indexPersonne(Personne p){
            int i = 0;		
            while(i < size-1 && personnes[i].compareTo(p) > 0) {	// Recherche de l'index à partir du début
                    i++;
            }	
            return i;
    }// indexPersonne  
        
    /* Amélioration de la méthode élémentaire 010 selon le principe de la sentinelle.
		Grâce à une comparaison préliminaire, la dernière clé (pour autant qu'elle existe !) peut servir
		de sentinelle.
		
		Évaluez l'ordre de grandeur du coût moyen et comparez avec les autres méthodes: 
		On observe que indexPersonne2() nous permet d'obtenir un gain de performance conséquent comparé à indexPersonne() dans les cas suivants:
			- si le tableau est vide (pas de boucle, retourne directement l'index 0)
			- si le nouvelle élément doit être placé à la fin du tableau (pas de boucle, retourne directement le dernier index)
		Dans le cas où le nouvelle élément doit être ailleur dans le tableau, le gain de performance n'est pas significatif, 
		cependant notre comparaison préliminaire nous permet de utiliser une "sentinelle". 
		Le gain de performance est bien présent et dû à l'utilisation d'une boucle simple (à une seule condition).

		Concerant insert011() par rapport à insert010(), la fonction est exactement la même
		*/
    public void insert011(Personne p){ 
            size++;
            int index = indexPersonne2(p);
            for(int i = size - 1; i >= index; i--){ // On connait l'index, on peut donc facilement faire un for...
                    personnes[i+1] = personnes[i];
            };	
            personnes[index] = p; // ... et place le nouveau directement à son index
    }// insert011  
    
    private int indexPersonne2(Personne p){
        if(size <= 1){ // Si le tableau est vide: le nouveau va à l'index 0
                return 0;
        } 
        if(personnes[size-2].compareTo(p) <= 0) { // Si le nouveau est plus petit que la dernière personne du tableau
                int i = 0;						// => le nouveau doit être mis quelque part au entre le début et la fin du tableau
                while(personnes[i].compareTo(p) >= 0) {	// Recherce de l'index à partir du début avec la sentinelle
                        i++;
                }
                return i;
        } else { // Sinon: on sait que le nouveau va à la dernière place du tableau
                return size-1;
        }
    }// indexPersonne2

}// TabOrdPers
    


public class HEG6331ATP03 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
	// Tests
        TabOrdPers tPers = new TabOrdPers(100);
        Personne x1 = new Personne("A", 7257);
            Personne x2 = new Personne("B", 7257);
            Personne x3 = new Personne("C", 5542);
            Personne x4 = new Personne("L", 2477);
            Personne x5 = new Personne("W", 6552);
        tPers.insert011(x1);
            tPers.insert011(x2);
            tPers.insert011(x3);
            tPers.insert011(x4);
            tPers.insert011(x5);	
            Personne p = new Personne("M", 2121);
            tPers.insert011(p);
            p = new Personne("A", 7259);
            tPers.insert011(p);
            p = new Personne("Z", 7259);
            tPers.insert011(p);
            p = new Personne("Aaa", 7252);
            tPers.insert011(p);
            System.out.println("actNbPers: " + tPers.size);
            System.out.println("tableau sorted: " + tPers.sorted());
            System.out.println("----------------------------");
            System.out.println("Liste des personnes: ");
            tPers.listerPersonnes();
    }
    
}
