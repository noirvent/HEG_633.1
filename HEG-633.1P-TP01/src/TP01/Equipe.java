package TP01;

import java.util.*;
/**
 * Tour du Canton de Genève
 *
 * Modélisation d'une équipe
 *
 * @author: Jonathan Blum
 *
*/
public class Equipe {

  private String nom;         /* Nom de l'équipe */
  private ArrayList coureurs;	/* Liste des coureurs de l'équipe */

  public Equipe (String nom) {
    /***** À COMPLÉTER *****/
      this.nom = nom;
      this.coureurs = new ArrayList();
  } // Constructeur

  /** Ajoute le Coureur c à la liste des coureurs de l'équipe */
  public void addCoureur (Coureur c) {
    /***** À COMPLÉTER *****/
      this.coureurs.add(c);
  } // addCoureur

  /** Retourne une représentation des informations de l'équipe sous la forme d'un String */
  public String toString () {
    /***** À COMPLÉTER *****/ 
   int type = this.getTypeEquipe();
   
   String typeEquipe;
   if (type < 0)
       typeEquipe = "Masculine";
   else if (type > 0)
       typeEquipe = "Féminine";
   else 
       typeEquipe = "Mixte";
   
   return "Équipe " + typeEquipe + " \"" + this.nom + "\", " + this.coureurs.size() + " coureurs, meilleur = " + this.getMeilleurCoureur();
  } // toString

  /***** À COMPLÉTER *****/
  
  /**
   * Retourne le type de l'équipe (Masculin, Mixte ou Féminin)
   * L'équipe est :
   *    - Masculine si le retour est négatif
   *    - Mixte si le retour est égal à 0
   *    - Féminine si le retour est positif
   */
  public int getTypeEquipe() {
      
      Iterator it = this.coureurs.iterator();
      
      boolean hommes =false, femmes = false;
      
      while(it.hasNext()) {
         Coureur c = (Coureur) it.next();
         if(c.isMasculin())
             hommes = true; 
         else
             femmes = true;
      }
      
      int type;
      
      if(hommes && !femmes)
          type = -1;
      else if(!hommes && femmes)
          type = 1;
      else if(hommes && femmes)
          type = 0;
      else
          type = 0; // Devrait retourner une exception "Équipe Vide"
      
      return type;
  }
  
  /** Retourne le coureur le plus rapide de l'équipe **/
  public Coureur getMeilleurCoureur() {
      
      Iterator it = this.coureurs.iterator();
     
      Coureur mc = (Coureur) it.next();
      
      while(it.hasNext()) {
          Coureur c = (Coureur) it.next();
          if(c.getTemps() < mc.getTemps()) 
              mc = c;
      }
      
      return mc;
  }

} // Equipe
