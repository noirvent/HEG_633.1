package TP01;

/**
 * Tour du Canton de Genève
 *
 * Modélisation d'un coureur
 *
 * @author: Jonathan Blum
 *
*/
public class Coureur {

  private String nom;       /* Nom du coureur */
  private String prenom;    /* Prénom du coureur */
  private boolean masculin; /* true si le sexe est masculin ("M"), false si le sexe est féminin ("F") */
  private double temps;     /* Temps réalisé */

  public Coureur (String nom, String prenom, String sexe, double temps) {
    /***** À COMPLÉTER *****/ 
      this.nom = nom;
      this.prenom = prenom;
      this.masculin = sexe.equals("M");
      this.temps = temps;
  } // Constructeur

  /** Retourne une représentation des informations du coureur sous la forme d'un String */
  public String toString () {
    /***** À COMPLÉTER *****/ 
    String sexe = (this.isMasculin()) ? "M" : "F";
    return this.nom + " " + this.prenom + " (" +  sexe + ") " + this.temps;
  } // toString

  /***** À COMPLÉTER *****/
  
  public boolean isMasculin() { return this.masculin; }
  
  public double getTemps() { return this.temps; }

} // Coureur