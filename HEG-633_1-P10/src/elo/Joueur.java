package elo;

/**
 * 633.1-Travaux pratiques TP P10
 * 
 * Classe modélisant un joueur
 * 
 * @author Jonathan Blum
 * @version 2.0
 */
public class Joueur implements Comparable {
    private String nom;
    private String prenom;
    private String pays;
    private int score;

    public Joueur(String nom, String prenom, String pays, int score) {
        this.nom = nom;
        this.prenom = prenom;
        this.pays = pays;
        this.score = score;
    }

    public String getNom() {return nom; }
    public void setNom(String nom) {this.nom = nom; }
    public String getPrenom() {return prenom; }
    public void setPrenom(String prenom) {this.prenom = prenom; }
    public String getPays() {return pays; }
    public void setPays(String pays) {this.pays = pays; }
    public int getScore() {return score; }
    public void setScore(int score) {this.score = score; }
    
  /** Méthode de comparaison: ordre lexicographique sur (nombre de points Elo; nom et prénom du joueur).
   * - Ordre sur les points Elo: numérique décroissant.
   * - Ordre sur les nom et prénom: alphabétique croissant.
   * C'est-à-dire: (nb1; nom1) <= (nb2; nom2) ssi nb1 > nb2 ou ((nb1 = nb2) et (nom1 <= nom2))
   */
  public int compareTo (Object obj) {
    Joueur j = (Joueur)obj;
    
    if(this.getScore() != j.getScore()) return j.getScore() - getScore();
    return (getNom() + getPrenom()).compareTo(j.getNom() + j.getPrenom());

  } // compareTo
  
  /* À COMPLÉTER SI NÉCESSAIRE */
  
    public String toWrite() {
        return nom+";"+prenom+";"+pays+";"+score;
    }

    public String toString() {
        return nom + " " + prenom + " - " + pays + " [" + score + "]";
    }

} // Joueur
