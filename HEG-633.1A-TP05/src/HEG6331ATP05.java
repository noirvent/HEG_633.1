
import java.util.ArrayList;
import java.util.Arrays;
import javax.lang.model.element.Element;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author jonathan
 */
public class HEG6331ATP05 {
    public static final int JoueursMax = 20;
    public static Joueur[] TableauDesScores = new Joueur[JoueursMax];
    public static int nbJoueurs = 0;
    public static class Joueur implements Comparable {
        String pseudo;
        int score;
        
        Joueur(String pseudo, int score) {
            this.pseudo = pseudo;
            this.score = score;
        }

        @Override
        public int compareTo(Object t) {
            int diffScore =  this.score - ((Joueur)t).score; // Plus grand score est avant
            if (diffScore == 0) {
                return ((Joueur)t).pseudo.compareTo(this.pseudo);
            } else return diffScore;
        }

        @Override
        public String toString() {
            return "Joueur{" + "pseudo=" + pseudo + ", score=" + score + '}';
        }         
    }

    /**
     * Recherche dichotomique simple 
     */
    public static void Insert20(Joueur j) {  
        int g, d, m, pos;
        int newSize = nbJoueurs +1; // Nouvelle taille du tableau
        if ( (newSize == 1 ) || (TableauDesScores[0].compareTo(j) < 0) ) {     // Si tableau pas vide & pas avant la première clef
            pos = 0;
        } else if (TableauDesScores[newSize-2].compareTo(j) >= 0) { // Si >= à la dernière valeur, on insert APRES
            pos = newSize-1;
        } else {    // Dans l'interval
            g = 0; d= newSize-2;
          
            // Invariant : g(indice) <= pos < d(indice) -> g < d -> g+1 <= d
            while ((g+1) < d) {
                m = (g+d) / 2;
                if(j.compareTo(TableauDesScores[m]) <= 0) {
                    g = m;
                } else {
                    d = m;
                }
            }
            pos = d;
        }
        // On décale... 
        for (int i = newSize-1; i >= pos; i--)
            TableauDesScores[i+1] = TableauDesScores[i];
        // On insert
        TableauDesScores[pos] = j;
        nbJoueurs++; // Pas oublier d'incrémenter le compte effectif
    }
   
    // Attribution de valeur fictives au cellules non consultées 
    /* Justification 
     * De notre tableau, seul la case dont l'indice est m est consultée.
     * Celle-ci étant comprise au sein de notre interval qui va progressivement
     * se ressérer sur m, on est donc sur que les bornes "inexistantes",
     * ne seront jamais consultées. */
    public static void Insert21(Joueur j) {
        int g, d, m, pos;
        int newSize = nbJoueurs +1; // Nouvelle taille du tableau   
        
        g = -1;
        d = newSize -1;
        // Invariant : g <= pos-1 < d; donc g < d; donc g+1 <= d
        while(g+1 < d) {
            m = (g+d) / 2;
            if(j.compareTo(TableauDesScores[m]) <= 0) {
                g=m;
            } else {
                d=m;
            }
        }
        pos = d;
        for(int i = newSize-1; i >= pos; i--)
            TableauDesScores[i+1] = TableauDesScores[i];
        TableauDesScores[pos] = j;
        nbJoueurs++; // Pas oublier d'incrémenter le compte effectif
    }
    public static void InsertPre3(Joueur j) {/*
        int g, d, m, pos;
        int newSize = nbJoueurs +1; // Nouvelle taille du tableau   
        
        g-1 = -1;
        d+1 = newSize -1;
        // Invariant : g <= pos-1 < d; donc g < d; donc g+1 <= d
        while(g-1+1 < d+1) {
            m = (g-1+d+1) / 2;
            if(j.compareTo(TableauDesScores[m]) <= 0) {
                g-1=m;
            } else {
                d+1=m;
            }
        }
        pos = d+1;
        for(int i = newSize-1; i >= pos; i--)
            TableauDesScores[i+1] = TableauDesScores[i];
        TableauDesScores[pos] = j;
        nbJoueurs++; // Pas oublier d'incrémenter le compte effectif      
    */}
    public static void Insert3(Joueur j) {
        int g, d, m, pos;
        int newSize = nbJoueurs +1; // Nouvelle taille du tableau   
        
        g = 0;
        d = newSize -2;
        // Invariant : g <= pos-1 < d; donc g < d; donc g+1 <= d
        // g-1+1 < d+1 donc g-1 < d donc g < d+1 donc g <= d
        while(g <= d) {
            m = (g+d) / 2;
            if(j.compareTo(TableauDesScores[m]) <= 0) {
                g=m+1;
            } else {
                d=m-1;
            }
        }
        pos = d+1; // équivalent à g
        for(int i = newSize-1; i >= pos; i--)
            TableauDesScores[i+1] = TableauDesScores[i];
        TableauDesScores[pos] = j;
        nbJoueurs++; // Pas oublier d'incrémenter le compte effectif          
    }
    public static int Mauchly(Joueur j) {
        int g, d, m, pos;
        int newSize = nbJoueurs +1; // Nouvelle taille du tableau  
        
        g = 0;
        d = newSize -2;
        // recherche
        while(g<=d) {
            m = (g+d) /2;
            if(j.compareTo(TableauDesScores[m]) <= 0) {
                g = m+1;
            } else {
                d = m-1;
            }
        }
        // Mais existe-t'il ou faut-il insérer ?
        if(d >= 0 && TableauDesScores[d].compareTo(j)== 0)
            return d;
        else
            return -g-1;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Initialisation du tableau        
        TableauDesScores[0] = new Joueur("Noirvent", 104570); nbJoueurs++;
        TableauDesScores[1] = new Joueur("Shiva", 104570); nbJoueurs++;
        TableauDesScores[2] = new Joueur("Shura", 104569); nbJoueurs++;
        TableauDesScores[3] = new Joueur("Dris", 39954); nbJoueurs++;
        TableauDesScores[4] = new Joueur("Ounga", 8374); nbJoueurs++;
        TableauDesScores[5] = new Joueur("Jerome", 2943); nbJoueurs++;
        
        // Insertions
        Insert20(new Joueur("Marcio", 8374));
        Insert20(new Joueur("Dom", 89330));
        Insert20(new Joueur("Capitch", 104570));
        Insert21(new Joueur("Fastolph", 82384));
        Insert21(new Joueur("Lyn", 3));
        Insert21(new Joueur("Sayu", 100000));
        Insert3(new Joueur("Coralie", 4586));
        Insert3(new Joueur("Jeremy", 1));
        Insert3(new Joueur("Olivier", 200000));
                
        // Affichage du tableau
        for (int i = 0; i < JoueursMax; i++) System.out.println(i+": " +TableauDesScores[i]);
        
        // Test Mauchly
        System.out.println("Sayu est en " + Mauchly(new Joueur("Sayu", 100000)));
        System.out.println("Coralie est en " + Mauchly(new Joueur("Coralie", 4586)));
        System.out.println("Diana devrait être insérée en " + Mauchly(new Joueur("Diana", 97484)));
        System.out.println("J-P devrait être insérée en " + Mauchly(new Joueur("J-P", 0)));
        System.out.println("Lucille devrait être insérée en " + Mauchly(new Joueur("Lucille", 999999)));

    }
}
