/**
 * TP07 633-1 Programmation Java
 * Calculateur IMC
 * @author Jonathan Blum
 */
import java.awt.*;
import java.awt.event.*;

public class IMC extends Frame implements ActionListener, TextListener {
    // Constantes
    private static final String TEXT_LB_IMC = "Votre IMC est ";
    private static final String TEXT_LB_CORP = "Votre corpulence est ";
    private static final String TEXT_CORP_INF16_5 = "malnutrition.";
    private static final String TEXT_CORP_16_5_18_5 = "maigreur.";
    private static final String TEXT_CORP_18_5_25 = "normale.";
    private static final String TEXT_CORP_SUP25 = "surpoid.";
    // Composants
    private final Label lbPoids = new Label("Poids (en kg)");
    private final TextField tfPoids = new TextField(10);
    private final Label lbTaille = new Label("Taille (en m)");
    private final TextField tfTaille = new TextField(10);
    private final Button btCalculer = new Button("Calculer");
    private final Button btNouveau = new Button("Nouveau");
    private final Button btFermer = new Button("Fermer");
    private final Label lbIMC = new Label("");
    private final Label lbCorp = new Label("");

    private IMC(String titre) {
        super(titre);
        initInterface();        
    }
    
    private void initInterface() {
        // Param. de la fenêtre
        setSize(250, 200);     
        setLayout(new FlowLayout());
        addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent we) {System.exit(0);}
            public void windowClosing(WindowEvent we) {((Frame)we.getSource()).dispose();}
        });   
        // Référencement des composants
        add(lbPoids);
        add(tfPoids);
        add(lbTaille);
        add(tfTaille);
        add(btCalculer);
        add(btNouveau);
        add(btFermer);
        add(lbIMC);
        add(lbCorp);
        // Listeners
        tfTaille.addTextListener(this);
        tfPoids.addTextListener(this);
        btCalculer.addActionListener(this);
        btNouveau.addActionListener(this);
        btFermer.addActionListener(this);
        // Etat initial
        resetInterface();
    }
    
    private void resetInterface() {
        tfPoids.setText("");
        tfTaille.setText("");
        lbIMC.setText("");
        lbCorp.setText("");
        btCalculer.setEnabled(false);
        validate();
    }
    
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource().equals(btNouveau)) 
            resetInterface();
        if(ae.getSource().equals(btFermer))
            dispose();
        if(ae.getSource().equals(btCalculer))
            Calculer();
    }
    
    public void textValueChanged(TextEvent te) {
         if(te.getSource().equals(tfPoids) || te.getSource().equals(tfTaille))
            validateSaisie();
    }    
    
    private void validateSaisie() {
        btCalculer.setEnabled((getPoids() > 0) && (getTaille() > 0));
    }
    
    private double getPoids() {
        try {
            return Double.parseDouble(tfPoids.getText());
        } catch (NumberFormatException e) {
            return -1;
        }        
    }
    private double getTaille() {
        try {
            return Double.parseDouble(tfTaille.getText());
        } catch (NumberFormatException e) {
            return -1;
        }         
    }
    private void Calculer() {
        double imc = getPoids() / Math.pow(getTaille(), 2);
        lbIMC.setText(TEXT_LB_IMC + imc);
        if(imc < 16.5) 
            lbCorp.setText(TEXT_LB_CORP + TEXT_CORP_INF16_5);
        else if(imc < 18.5)
            lbCorp.setText(TEXT_LB_CORP + TEXT_CORP_16_5_18_5);
        else if(imc < 25 ) 
            lbCorp.setText(TEXT_LB_CORP + TEXT_CORP_18_5_25);
        else
            lbCorp.setText(TEXT_LB_CORP + TEXT_CORP_SUP25);
        validate();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new IMC("Indice de masse corporelle").setVisible(true);
    }
}
