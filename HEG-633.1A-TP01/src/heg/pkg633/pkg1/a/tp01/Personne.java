/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heg.pkg633.pkg1.a.tp01;

/**
 *
 * @author jonathan
 */
class Personne implements Comparable<Object> {
    String name;

    public Personne(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Object t) {
        return this.name.compareTo((String) t);
    }

}
