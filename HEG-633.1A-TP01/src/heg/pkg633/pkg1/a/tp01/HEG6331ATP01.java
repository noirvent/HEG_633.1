/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heg.pkg633.pkg1.a.tp01;

import java.util.Date;

/**
 *
 * @author jonathan
 */

public class HEG6331ATP01 {
    
    public static int Comp(int v1, int v2) {
        int res = v1 - v2;
        return (res != 0) ? res : 0;
    }
    
    public static float Comp(float v1, float v2) {
        float res = v1 - v2;
        return (res != 0) ? res : 0;
    }    
    
    public static int Comp(char v1, char v2) {
        int res = (int) v1 - (int) v2;
        return (res != 0) ? -(res) : 0;
    }
    
    public static int Comp(Date v1, Date v2) {
        int resY = v1.getYear()- v2.getYear();
        if(resY == 0) {
            int resM = v1.getMonth() - v2.getMonth();
            if(resM == 0) {
                int resD = v1.getYear() - v1.getYear();
                return resD;
            } else return resM;
        } else return resY;
            
    }
    
    public static int Comp(String v1, String v2) {
        return 0;
    }
    
    public static int Comp(Comparable v1, Comparable v2) {
        return v1.compareTo(v2);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("tests comp int");
        System.out.println(Comp(38, 24));
        System.out.println(Comp(27, 42));
        System.out.println(Comp(22, 22));
        
        System.out.println("tests comp float");
        System.out.println(Comp(38.6F, 24.9F));
        System.out.println(Comp(27.2F, 42.48F));
        System.out.println(Comp(22.F, 22.F));     
        
        System.out.println("tests comp char");
        System.out.println(Comp('a', 'z'));
        System.out.println(Comp('y', 'b'));
        System.out.println(Comp('g', 'g'));       
        
        System.out.println("tests comp date");
        System.out.println(Comp(new Date(2013,11,18), new Date()));
        System.out.println(Comp(new Date(2014,3,22), new Date(2014,11,31)));
        System.out.println(Comp(new Date(), new Date()));        
        
        
        System.out.println("tests comp Comparable");
        
        
        //System.out.println(Comp(new String('Jonathan'), new String('Michel')));
        //System.out.println(Comp(new Personne('Peter'), new Personne('André')));
        //System.out.println(Comp(new Personne('Gaston'), new Personne('Gaston'))); 
    }

    
}
