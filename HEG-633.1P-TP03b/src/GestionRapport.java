import java.util.*;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
/**
 * Module 633.1-Programmation - TP P03
 *
 * Application de gestion d'un distributeur automatique
 *
 * Gestion de l'automate de la cafétéria.
 *
 * @author Jonathan Blum
*/
public class GestionRapport {

  /** Liste des délimiteurs */
  private static final String DEL_ARTICLE = "\n\r";
  private static final String DEL_CHAMP = ";";

  private static Distributeur distributeur; /* Le distributeur automatique */

  /** Convertit une chaîne de caractères en int / en double (pour simplifier le code) */
  private static int str2int (String s) {return Integer.parseInt (s);}
  private static double str2double (String s) {return Double.parseDouble (s);}
  
  /** Mémorise les données du distributeur automatiques à partir du fichier de données stocké dans str (rayons et articles) */
  private static void memoriseDistributeur (String str) {

    /*** À COMPLÉTER ***/
    StringTokenizer lines = new StringTokenizer(str, DEL_ARTICLE);
    while(lines.hasMoreTokens()) {
        StringTokenizer line = new StringTokenizer(lines.nextToken(), DEL_CHAMP);     
        // N° Distributeur
        if(distributeur == null) 
            distributeur = new Distributeur(str2int(line.nextToken()));
        else
            line.nextToken();
        // Rayon
        int noRayon = str2int(line.nextToken());
        Rayon r = distributeur.get(noRayon);
        if(r == null) {
            r = new Rayon(noRayon); distributeur.add(r);
        }
        // Article, données
        int noArticle = str2int(line.nextToken());
        String nomArticle = line.nextToken();
        int typeArticle = str2int(line.nextToken());
        line.nextToken(); // description, non utilisée
        double prArticle = str2double(line.nextToken());
        double pvArticle = str2double(line.nextToken());
        double qtArticle = str2double(line.nextToken());  
        // Articles, ajout
        Article a;
        if (typeArticle == 0) 
            a = new Boisson(noArticle, nomArticle, typeArticle, prArticle, pvArticle, qtArticle);
        else
            a = new Nourriture(noArticle, nomArticle, typeArticle, prArticle, pvArticle, qtArticle);
        r.add(a);
    }
  } // memoriseDistributeur

  /** Bénéfice total possible avec le contenu du distributeur */
  private static double beneficeTotal () {return distributeur.benefice();} 

  /** Prix max des articles du distributeur */
  private static double prixMax () {return distributeur.prixMax();} 

  /** Calcule et affiche le rapport */
  private static void afficheRapport () {
    /*** À COMPLÉTER ***/
      System.out.println("Rapport de l'automatique n°1");
      System.out.println("  Le prix max de tous les articles est : " + prixMax());
      System.out.println("  Le bénéfice total possible est: " + beneficeTotal()+ " CHF");     
      System.out.println("  Nombre d'articles par rayon:");
      for (Iterator iterator = distributeur.getRayons().iterator(); iterator.hasNext();) {
          Rayon r = (Rayon) iterator.next();      
          System.out.println("    rayon n°" + r.getNo() + " contient " + ((r.getArticles().size() <= 1) ? "A REMPLIR" : (r.getArticles().size() + " articles")));
      }
      // La suite n'étais pas clairement définie dans l'énoncé
      System.out.println("----------------------------------------------------------------");
      afficheDistributeur();
  } // afficheRapport

  /** Affiche le contenu du distributeur  (tous les rayons avec tous leurs articles) */
  private static void afficheDistributeur () {
      System.out.print(distributeur);
      
  } // afficheDistributeur

  /** Méthode principale de l'application. */
  public static void main (String[] args) {
    if (args.length < 1) { 
    	System.out.println("Vous devez préciser le nom du fichier de données.");
    }	else {
      System.out.println("Traitement des arrivages (fichier : \"" + args[0] + "\")");
      memoriseDistributeur(FileToStr.read(args[0]));
      afficheRapport();
    }
  } // main

} // GestionRapport
