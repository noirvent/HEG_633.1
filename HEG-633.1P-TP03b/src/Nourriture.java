/**
 * Module 633.1-Programmation - TP P03
 *
 * Application de gestion d'un distributeur automatique
 *
 * Modélisation d'un article de catégoie "Nourriture" vendu dans le distributeur.
 *
 * @author VOTRE NOM
*/
public class Nourriture extends Article {
  private double poids;  /* Poids */

  /** Constructeur */
  public Nourriture (int no, String nom ,int type, double prixRevient, double prixVente, double poids) {
    super(no, nom ,type, prixRevient, prixVente);
    this.poids = poids;
  } // Constructeur

  /** Accesseurs */
  public double getPoids () {return poids;}

  /** Représente les données de la nourriture sous la forme d'un String */
  public String toString () {
    return "\nNourriture " + " no : " + no + " nom : " + nom + " type : " + type 
           + " poids : " + poids 
           + " (prix de revient : " + prixRevient + " prix de vente : " + prixVente + ").";
  } // toString

} // Nourriture