import java.util.*;
/**
 * Module 633.1-Programmation - TP P03
 *
 * Application de gestion d'un distributeur automatique
 *
 * Modélisation d'un Distributeur contenant des rayons avec leurs articles.
 *
 * La classe Distributeur mémorise tous les rayons du distributeur.
 *
 * @author VOTRE NOM
*/
public class Distributeur {

  private int no;           /* no du Distributeur (identifiant) */
  private ArrayList rayons; /* Collection de tous les rayons du distributeur */

  /** Constructeur */
  public Distributeur (int no) {
    this.no = no;
    rayons = new ArrayList();
  } // Constructeur

  /** Accesseurs */
  public int getNo () {return no;}
  public ArrayList getRayons () {return rayons;}

  /** Ajoute la référence d'un rayon au distributeur */
  public void add (Rayon rayon) {
    /*** À COMPLÉTER ***/
      rayons.add(rayon);
  } // add

  /** Retourne la référence du Rayon d'identifiant noRayon 
      Retourne null si le rayon n'existe pas. */
  public Rayon get (int noRayon) {
    /*** À COMPLÉTER ***/
    int search = rayons.indexOf(new Rayon(noRayon));
    if(search >= 0)
        return (Rayon) rayons.get(search);
    else 
        return null;
  } // get

  /** Retourne le prix le plus élevé des articles du distributeur */
  public double prixMax () {
    /*** À COMPLÉTER ***/
      double prixMax = 0.0, prixNext;
      for (Iterator iterator = rayons.iterator(); iterator.hasNext();)
          prixMax = ((prixNext = ((Rayon) iterator.next()).prixMax()) >= prixMax) ? prixNext : prixMax;
      return prixMax;
  } // prixMax

  /** Calcule et retourne le bénéfice total possible du contenu du distributeur */
  public double benefice () {
    /*** À COMPLÉTER ***/
      double total = 0.0;
      for (Iterator iterator = rayons.iterator(); iterator.hasNext();)
          total += ((Rayon) iterator.next()).benefice();
      return total;
  } // benefice

  /** Deux distributeurs sont identiques si leur identifiant sont identiques */
  public boolean equals (Object obj) {
    return this.no == ((Distributeur)obj).no;
  } // equals

  /** Représente les données du distributeur sous la forme d'un String */
  public String toString () {
    return "\nDistributeur " + " no : " + no + " rayon : " + rayons;
  } // toString

} // Distributeur