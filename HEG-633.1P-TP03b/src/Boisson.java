/**
 * Module 633.1-Programmation - TP P03
 *
 * Application de gestion d'un distributeur automatique
 *
 * Modélisation d'un article de catégoie "Boisson" vendu dans le distributeur.
 *
 * @author VOTRE NOM
*/
public class Boisson extends Article {

  private double contenance;  /* Contenance */

  /** Constructeur */
  public Boisson (int no, String nom ,int type, double prixRevient, double prixVente, double contenance) {
    super(no, nom ,type, prixRevient, prixVente);
    this.contenance = contenance;
  } // Constructeur

  /** Accesseurs */
  public double getContenance () {return contenance;}

  /** Représente les données de la boisson sous la forme d'un String */
  public String toString () {
    return "\nBoisson " + " no : " + no + " nom : " + nom + " type : " + type 
           + " contenance : " + contenance 
           + " (prix de revient : " + prixRevient + " prix de vente : " + prixVente + ").";
  } // toString

} // Boisson

