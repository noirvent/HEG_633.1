/**
 * Module 633.1-Programmation - TP P03
 *
 * Application de gestion d'un distributeur automatique
 *
 * Modélisation d'un article vendu dans le distributeur.
 *
 * La classe article définit les objets à vendre dans les rayons du distributeur. 
 *
 * @author VOTRE NOM
*/
public class Article {

  /** Les types d'articles possibles */
  public static final int TYPE_INCONNU = -1;
  public static final int TYPE_BOISSON = 0;
  public static final int TYPE_NOURRITURE = 1;

  protected int no;             /* no du Article (identifiant) */
  protected String nom;         /* Nom de l'article */
  protected int type;           /* Type de l'article (-1: inconnu, 0: boisson, 1: nourriture)*/
  protected double prixRevient; /* Prix de revient (prix d'achat au grossiste) */
  protected double prixVente;   /* Prix de vente au client */

  /** Constructeur */
  public Article (int no, String nom, int type, double prixRevient, double prixVente) {
    this.no = no;
    this.nom = nom;
    this.type = type;
    this.prixRevient = prixRevient;
    this.prixVente = prixVente;
  } // Constructeur

  /** Constructeur */
  public Article (int no) {
    this.no = no;
  } // Constructeur

  /** Accesseurs */
  public int getNo () {return no;}
  public String getNom () {return nom;}
  public int getType () {return type;}
  public double getPrixRevient () {return prixRevient;}
  public double getPrixVente () {return prixVente;}

  /** Retourne le bénéfice effectué lors de la vente de l'article */
  public double benefice () {
    return prixVente - prixRevient;
  } // benefice

  /** Deux articles sont identiques si leur identifiant est identique */
  public boolean equals (Object obj) {
    return this.no == ((Article)obj).no;
  } // equals

  /** Représente les données de l'article sous la forme d'un String */
  public String toString () {
    return "\nArticle " + " no : " + no + " nom : " + nom + " type : " + type 
           + " (prix de revient : " + prixRevient + " prix de vente : " + prixVente + ").";
  } // toString

} // Article