import java.util.*;
/**
 * Module 633.1-Programmation - TP P03
 *
 * Application de gestion d'un distributeur automatique
 *
 * Modélisation d'un Rayon contenant des articles (Boissons ou Nourriture).
 *
 * La classe Rayon mémorise l'ensemble des articles présents sur le rayon.
 *
 * @author VOTRE NOM
*/
public class Rayon {

  private int no;             /* no du Rayon (identifiant) */
  private ArrayList articles; /* Collection de tous les articles du rayon */

  /** Constructeur */
  public Rayon (int no) {
    this.no = no;
    articles = new ArrayList();
  } // Constructeur

  /** Accesseurs */
  public int getNo () {return no;}
  public ArrayList getArticles () {return articles;}

  /** Ajoute la référence d'un article dans le rayon. */
  public void add (Article article) {
    articles.add(article);
  } // add

  /** Un achat consiste à supprimer l'article du rayon */
  public void achat (int noArticle) {
     articles.remove(noArticle);
  } // achat

  public void achat (Article article) {
     articles.remove(article);
  } // achat

  /** Retourne la référence de l'article identifié par noArticle
      Retourne null si l'article n'existe pas.*/
  public Article get (int noArticle) {
    int pos = articles.indexOf(new Article(noArticle));
    if (pos < 0) {return null;}
    return (Article)articles.get(pos);
  } // get

  /** Rend le nombre d'articles présents sur le rayon */
  public int nbArticles () {
    return articles.size();
  } // nbArticles

  /** Retourne le prix total des articles présents sur le rayon */
  public double prixTotal () {
    double prix = 0.0;
    Iterator it = articles.iterator();
    while (it.hasNext()) {
      prix += ((Article)it.next()).getPrixVente();
    }
    return prix;
  } // prixTotal

  /** Retourne le prix le plus élevé des articles du rayon */
  public double prixMax () {
    double max = 0.0;
    Iterator it = articles.iterator();
    while (it.hasNext()) {
      double p = ((Article)it.next()).getPrixVente();
      if (max < p) {max = p;}    
    }
    return max;
  } // prixMax

  /** Retourne le bénéfice total des articles présents sur le rayon */
  public double benefice () {
    double b = 0.0;
    Iterator it = articles.iterator ();
    while (it.hasNext()) { 
      b += ((Article)it.next()).benefice();
    }
    return b;
  } // benefice 

  /** Retourne true ssi le rayon est vide. */
  public boolean estVide () {
    return articles.size() == 0;
  } // estVide       

  /** Deux Rayons sont identiques si leur identifiant est identique */
  public boolean equals (Object obj) {
    return this.no == ((Rayon)obj).no;
  } // equals

  /** Représente les données du Rayon sous la forme d'un String */
  public String toString () {
    return "\nRayon " + " no : " + no + " rayon : " + articles;
  } // toString

} // Rayon