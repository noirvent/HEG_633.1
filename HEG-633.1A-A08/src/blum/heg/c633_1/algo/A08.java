package blum.heg.c633_1.algo;

/**
 *
 * @author Jonathan Blum
 */
public class A08 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Samples
        Joueur j1 = new Joueur("Noirvent", 104570); 
        Joueur j2 = new Joueur("Shiva", 104570); 
        Joueur j3 = new Joueur("Shura", 104569);
        Joueur j4 = new Joueur("Dris", 39954); 
        Joueur j5 = new Joueur("Ounga", 8374); 
        Joueur j6 = new Joueur("Jerome", 2943);
        Joueur j7 = new Joueur("Marcio", 8374);
        Joueur j8 = new Joueur("Dom", 89330);
        Joueur j9 = new Joueur("Capitch", 104570);
        Joueur j10 = new Joueur("Fastolph", 82384);
        Joueur j11 = new Joueur("Lyn", 3);
        Joueur j12 = new Joueur("Sayu", 100000);
        Joueur j13 = new Joueur("Coralie", 4586);
        Joueur j14 = new Joueur("Jeremy", 1);
        Joueur j15 = new Joueur("Olivier", 200000);
                
        TableauScores scores = new TableauScores(20);
        scores.add(j1);
        scores.add(j2);
        scores.add(j3);
        scores.add(j4);
        scores.add(j5);
        scores.add(j6);
        scores.add(j7);
        scores.add(j8);
        scores.add(j9);
        scores.add(j10);
        scores.add(j11);
        scores.add(j12);
        scores.add(j13);
        scores.add(j14);
        scores.add(j15);
        scores.shuffle();
        scores.print();
        System.out.println("---");
        
        // tests
        
        // Shell sort
        TableauScores score_ex1 = new TableauScores(scores);
        score_ex1.ShellSort();
        score_ex1.print();
        System.out.println("---");
        
        // Shell sort Alternatif
        TableauScores score_ex2 = new TableauScores(scores);
        score_ex2.ShellSortAlt();
        score_ex2.print();
        System.out.println("---");        
        
        // Heap Sort
        TableauScores score_ex3 = new TableauScores(scores);
        score_ex3.HeapTreeSort();
        score_ex3.print();
        System.out.println("---");           
        
        
        System.out.println((j1.compareTo(j2)<0) ? "J14 est strictement avant J15" : "J1 est égal ou après j3");
    }
}
