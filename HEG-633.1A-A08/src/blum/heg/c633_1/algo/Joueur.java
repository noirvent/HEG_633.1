package blum.heg.c633_1.algo;

/**
 *
 * @author Jonathan Blum
 */
public class Joueur implements Comparable {
        String pseudo;
        int score;
        
        Joueur(String pseudo, int score) {
            this.pseudo = pseudo;
            this.score = score;
        }

        @Override
        public int compareTo(Object t) {
            int diffScore =  ((Joueur)t).score - this.score; // Plus grand score est avant
            if (diffScore == 0) {
                return ((Joueur)t).pseudo.compareTo(this.pseudo);
            } else return diffScore;
        }

        @Override
        public String toString() {
            return "Joueur{" + "pseudo=" + pseudo + ", score=" + score + '}';
        }         
    }