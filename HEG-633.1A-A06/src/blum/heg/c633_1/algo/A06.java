package blum.heg.c633_1.algo;

/**
 *
 * @author Jonathan Blum
 */
public class A06 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Samples
        Joueur j1 = new Joueur("Noirvent", 104570); 
        Joueur j2 = new Joueur("Shiva", 104570); 
        Joueur j3 = new Joueur("Shura", 104569);
        Joueur j4 = new Joueur("Dris", 39954); 
        Joueur j5 = new Joueur("Ounga", 8374); 
        Joueur j6 = new Joueur("Jerome", 2943);
        Joueur j7 = new Joueur("Marcio", 8374);
        Joueur j8 = new Joueur("Dom", 89330);
        Joueur j9 = new Joueur("Capitch", 104570);
        Joueur j10 = new Joueur("Fastolph", 82384);
        Joueur j11 = new Joueur("Lyn", 3);
        Joueur j12 = new Joueur("Sayu", 100000);
        Joueur j13 = new Joueur("Coralie", 4586);
        Joueur j14 = new Joueur("Jeremy", 1);
        Joueur j15 = new Joueur("Olivier", 200000);
                
        /**
         * Série A05
         */
        TableauScores scores = new TableauScores(20);
        scores.Insert20(j1);
        scores.Insert20(j2);
        scores.Insert20(j3);
        scores.Insert20(j4);
        scores.Insert20(j5);
        scores.Insert21(j6);
        scores.Insert21(j7);
        scores.Insert21(j8);
        scores.Insert21(j9);
        scores.Insert21(j10);
        scores.Insert3(j11);
        scores.Insert3(j12);
        scores.Insert3(j13);
        scores.Insert3(j14);
        scores.Insert3(j15);
          
        //scores.print();
        
        // Test Mauchly       
        //System.out.println("Sayu est en " + scores.Mauchly(j12));
        //System.out.println("Coralie est en " + scores.Mauchly(j13));
        //System.out.println("Diana devrait être insérée en " + scores.Mauchly(new Joueur("Diana", 97484)));
        //System.out.println("J-P devrait être insérée en " + scores.Mauchly(new Joueur("J-P", 0)));
        //System.out.println("Lucille devrait être insérée en " + scores.Mauchly(new Joueur("Lucille", 999999))); 
        
        /**
         * Série A06
         */
        TableauScores scores2 = new TableauScores(20);
        scores2.add(j1);
        scores2.add(j2);
        scores2.add(j3);
        scores2.add(j4);
        scores2.add(j5);
        scores2.add(j6);
        scores2.add(j7);
        scores2.add(j8);
        scores2.add(j9);
        scores2.add(j10);
        scores2.add(j11);
        scores2.add(j12);
        scores2.add(j13);
        scores2.add(j14);
        scores2.add(j15);
        scores2.print();
        System.out.println("---");
        
        // test 
        //scores2.tri20();
        //scores2.tri21();
        //scores2.tri3(); doesn't work
        scores2.print();
        

    }
}
